from Getch import getch
from GridLogos import logos
import time
import os
import random
import select
import sys


gamespeed = 0.04


def clear_screen():
    if os.name == 'posix':
        # clear
        os.system("clear")
        print("clear")
    elif os.name == 'nt':
        os.system("cls")
        print("cls")


class Player:

    def __init__(self, x_location, y_location, state, health, sign, mapa):
        self.x_original = x_location
        self.y_original = y_location
        self.health_original = health
        self.state_original = state
        self.sign_original = sign
        self.x = x_location
        self.y = y_location
        self.state = state
        self.health = health
        self.sign = sign
        self.mapa = mapa

    def restart(self):
        self.x = self.x_original
        self.y = self.y_original
        self.state = self.state_original
        self.health = self.health_original
        self.sign = self.sign_original

    def is_player_here(self, x, y):
        if self.state == "stand":
            if x == self.x and y == self.y:
                return True
            if x == self.x and y == self.y - 1:
                return True
        if self.state == "slide":
            if x == self.x and y == self.y:
                return True
        if self.state == "jump":
            if x == self.x and y == self.y - 1:
                return True
            if x == self.x and y == self.y - 2:
                return True

    def move(self, direction):
        if direction == 'w' and self.y - 1 >= 0:
            self.y -= 1
            self.state = "jump"
           # print_2D_grid(matrix, player_location)
            # time.sleep(2)
            self.state = "stand"
           # print_2D_grid(matrix, player_location)

        elif direction == 's' and self.y + 1 < len(self.mapa.matrix[0]):
            self.state = "slide"
            self.y += 1
            # print_2D_grid(matrix, player_location)
            # time.sleep(2)
            self.state = "stand"
           # print_2D_grid(matrix, player_location)
        # elif direction == 'a' and player_location[0] - 1 >= 0:
        #    player_location[0] -= 1
        # elif direction == 'd' and player_location[0] + 1 < len(matrix[0]):
         #   player_location[0] += 1


class Game:

    def __init__(self, mapa, player):
        self.mapa = mapa
        self.players = []
        self.players.append(player)

    def print_map(self):
        self.mapa.print_2D_grid()

    def restart(self):
        self.mapa.restart()
        for x in range(0, len(self.players)):
            self.players[x].restart()

    def player_jump(self, player):
        player.y -= 1
        self.print_map()
        self.mapa.matrix = self.mapa.matrix[1:]
        self.mapa.matrix.append(self.mapa.matrix_empty[:])
        self.check_collisions()
        time.sleep(gamespeed * 2)
        self.print_map()
        self.mapa.matrix = self.mapa.matrix[1:]
        self.mapa.matrix.append(self.mapa.matrix_empty[:])
        self.check_collisions()
        time.sleep(gamespeed * 2)
        self.print_map()
        self.mapa.matrix = self.mapa.matrix[1:]
        self.mapa.matrix.append(self.mapa.matrix_empty[:])
        self.check_collisions()
        time.sleep(gamespeed * 2)
        player.y += 1
        self.print_map()
        self.mapa.matrix = self.mapa.matrix[1:]
        self.mapa.matrix.append(self.mapa.matrix_empty[:])
        self.check_collisions()

        if random.randint(0,8)%2==0:
            self.mapa.generate_random_obstacles()
            self.mapa.add_obstacles_to_map()

    def player_slide(self, player):
        temp = player.sign
        player.sign = "-"
        player.y += 1
        self.print_map()
        self.mapa.matrix = self.mapa.matrix[1:]
        self.mapa.matrix.append(self.mapa.matrix_empty[:])
        self.check_collisions()
        time.sleep(gamespeed * 2)
        self.print_map()
        self.mapa.matrix = self.mapa.matrix[1:]
        self.mapa.matrix.append(self.mapa.matrix_empty[:])
        self.check_collisions()
        time.sleep(gamespeed * 2)
        self.print_map()
        self.mapa.matrix = self.mapa.matrix[1:]
        self.mapa.matrix.append(self.mapa.matrix_empty[:])
        self.check_collisions()
        time.sleep(gamespeed * 2)
        player.sign = temp
        player.y -= 1
        self.print_map()
        self.mapa.matrix = self.mapa.matrix[1:]
        self.mapa.matrix.append(self.mapa.matrix_empty[:])
        self.check_collisions()


        if random.randint(0,8)%2==0:
            self.mapa.generate_random_obstacles()
            self.mapa.add_obstacles_to_map()

    def check_collisions(self):
        for x in range(0, len(self.players)):
            if self.mapa.matrix[self.players[x].x][self.players[x].y] != "." and self.mapa.matrix[self.players[x].x][self.players[x].y] != "_":
                logos(5)
                self.restart()
                #print("xD")


class Grid2D:
    xsize = 0
    ysize = 0
    matrix = []
    players = []
    obstacles = []
    previous = -1
    counter = 0
    matrix_empty = []

    def __init__(self, x, y):
        self.xsize = x
        self.ysize = y
        self.matrix = self.generate_2D_grid(self.xsize, self.ysize)

    def generate_2D_grid(self, x, y):
        xsize = []

        matrix = ["." for y in range(0, y)]

        floor = ["_" for y in range(0, y)]

        for i in range(0, x):
            xsize.append(matrix[:])

        for i in range(0, x):
            xsize[i][int(round(y / 2 + 1))] = "_"
            xsize[i][int(round(y / 2 - 2))] = "_"

        self.matrix_empty = matrix[:]
        self.matrix_empty[int(round(y / 2 + 1))] = "_"
        self.matrix_empty[int(round(y / 2 - 2))] = "_"

       # for x in range(0,x):
      #  for y in range (0,y):
        #    if y==player_location[1]:
        #     matrix[x][y]='_'
        return xsize

    def restart(self):
        self.matrix = self.generate_2D_grid(self.xsize, self.ysize)

    def connect_player(self, player):
        self.players.append(player)

    # def change_character(self,string, position, char_to_change_to):
     #   to_return = ""
      #  temp = list(string)
       # temp[position] = char_to_change_to
        # for i in temp:
        #   to_return += temp[i]
        # return to_return

    def generate_random_obstacles(self):
        self.obstacles = self.matrix_empty[:]
        if self.previous != 0 and self.previous != 1 and self.counter > 2:
            a = random.randint(0, 8)
        else:
            a = -1
            self.counter += 2

        if a == 0:
            self.obstacles[int(round(self.ysize / 2))] = "#"
            self.counter = 0
        if a == 1:
            self.obstacles[int(round(self.ysize / 2 - 1))] = "#"
            self.counter = 0
        if a == 2:
            self.obstacles[int(round(self.ysize / 2 + 1))] = "#"
            self.counter = 0

        self.previous = a

    def add_obstacles_to_map(self):
        temp = []
        for x in range(1, len(self.matrix)):
            temp.append(self.matrix[x][:])
        temp.append(self.obstacles)
        self.matrix = temp

    def organize_grid(self):
        matrix_temp = []
        for x in range(0, len(self.matrix)):
            matrix_temp.append(self.matrix[x][:])

        for i in range(0, len(self.players)):
            matrix_temp[self.players[i].x][
                self.players[i].y] = self.players[i].sign

        return matrix_temp

    def print_2D_grid(self):

        clear_screen()

        matrix_temp = self.organize_grid()

        wall = ""
        for i in range(0, len(self.matrix) + 2):
            wall += "#"

        print(wall)
        to_be_printed = []
        # map printing
        temp = "#"

        for y in range(0, len(matrix_temp[0])):
            for x in range(0, len(matrix_temp)):
                temp += str(matrix_temp[x][y])
            temp += "#"
            print(temp)
            temp = "#"

        print(wall)

        print(self.players[0].x, self.players[0].y)

xsize = 20
ysize = 10

mapa = Grid2D(xsize, ysize)
player1 = Player(2, int(round(ysize / 2)), "stand", 3, "x", mapa)

game = Game(mapa, player1)

mapa.connect_player(player1)

# player_location = [2, int(round(ysize / 2)), "stand"]  # initial settings
# matrix = generate_2D_grid(xsize, ysize, player_location)  # initial settings


logos(1)  # displays first menu entry

# window=curses.initscr()
# window.timeout(500)
# curses.noecho()

# a=select.select([sys.stdin.read(1)],[],[],5)
i = 0
a = ""
while a != 'q':
    # mapa.print_2D_grid()

    if i % 5 == 0:
        mapa.generate_random_obstacles()
        mapa.add_obstacles_to_map()

    game.print_map()
    game.check_collisions()
    print("TIMEPASS" + str(i))
    # try:
    a = getch(gamespeed)

    # except:
    #   print("noinput")
    if a != "q" and a != "p":
        if a == "w" or a == " ":
            game.player_jump(player1)
        elif a == "s":
            game.player_slide(player1)

        # player1.move(a)
    if a == "p" or a == chr(033):
        logos(6)
    i += 1

# os.system("reset")
